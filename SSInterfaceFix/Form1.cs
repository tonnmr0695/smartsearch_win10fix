﻿using SSInterfaceFix.Properties;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace SSInterfaceFix
{
    public partial class SmartSearchWin10Fix : Form
    {
        public SmartSearchWin10Fix()
        {
            InitializeComponent();
        }

        public string QRes { get; private set; }
        public object Mode { get; private set; }

        private void BtnYes_Click(object sender, EventArgs e)
        {
            //Kill SSInterface if running
            try
            {
                foreach (Process proc in Process.GetProcessesByName("SSInterface"))
                {
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //Delete Smartsearch Directory
            if (Directory.Exists(Environment.GetEnvironmentVariable("LocalAppData") + @"\Square_9_Softworks"))
            {
                Directory.Delete(Environment.GetEnvironmentVariable("LocalAppData") + @"\Square_9_Softworks", true);
            }

            //Write screen resolution
            var w = new System.Diagnostics.Process();
            w.StartInfo.FileName = "cmd.exe";
            w.StartInfo.Arguments = @"/c wmic desktopmonitor get screenwidth > C:\TEMP\SmartSearch\width.txt";
            w.StartInfo.RedirectStandardOutput = true;
            w.StartInfo.UseShellExecute = false;
            w.StartInfo.CreateNoWindow = true;
            w.Start();

            var h = new System.Diagnostics.Process();
            h.StartInfo.FileName = "cmd.exe";
            h.StartInfo.Arguments = @"/c wmic desktopmonitor get screenheight > C:\TEMP\SmartSearch\height.txt";
            h.StartInfo.RedirectStandardOutput = true;
            h.StartInfo.UseShellExecute = false;
            h.StartInfo.CreateNoWindow = true;
            h.Start();

            if (!Directory.Exists(@"C:\TEMP\SmartSearch"))
            {
                Directory.CreateDirectory(@"C:\TEMP\SmartSearch");
            }

            File.WriteAllBytes(@"C:\TEMP\SmartSearch\QRes.exe", Resources.QRes);
            File.WriteAllBytes(@"C:\TEMP\SmartSearch\SmartSearchResetRes.exe", Resources.SmartSearchResetRes);

            CreateShortcut("SmartSearchFix", Environment.GetFolderPath(Environment.SpecialFolder.Startup), Assembly.GetExecutingAssembly().Location);

            System.Diagnostics.Process.Start("ms-settings:display");
            Thread.Sleep(1000);
            AppActivate("Nastavení"); //Czech expression for Settings

            SendKeys.SendWait("{TAB}");
            Thread.Sleep(100);
            SendKeys.SendWait("{TAB}");
            Thread.Sleep(100);
            SendKeys.SendWait("{TAB}");
            Thread.Sleep(100);
            SendKeys.SendWait("{TAB}");
            Thread.Sleep(100);
            SendKeys.SendWait("{TAB}");
            Thread.Sleep(100);
            SendKeys.SendWait("{UP}");
            Thread.Sleep(100);
            Thread.Sleep(100);
            SendKeys.SendWait("{UP}");
            Thread.Sleep(100);
            Thread.Sleep(100);
            SendKeys.SendWait("{UP}");
            Thread.Sleep(100);
            Thread.Sleep(100);
            SendKeys.SendWait("{UP}");

            //Start QRes and then pass the width and height.
            Process process = new System.Diagnostics.Process();
            ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = @"C:\TEMP\SmartSearch\QRes.exe";
            startInfo.Arguments = "/x:" + "1920" + ' ' + "/y:" + "1080";
            process.StartInfo = startInfo;
            process.Start();

            //Message Success
            Process.Start("cmd", "/c shutdown -l");
        }

        [DllImportAttribute("User32.dll")]
        private static extern int SetForegroundWindow(int hWnd);

        private static bool AppActivate(string titleName)
        {
            var success = true;
            var process = Process.GetProcesses()
                          .Where(p => p.MainWindowTitle.StartsWith(titleName))
                          .FirstOrDefault();
            if (process != null)
            {
                SetForegroundWindow(process.MainWindowHandle.ToInt32());
            }
            else
            {
                success = false;
            }
            return success;
        }

        public static void CreateShortcut(string shortcutName, string shortcutPath, string targetFileLocation)
        {
            string shortcutLocation = System.IO.Path.Combine(shortcutPath, shortcutName + ".lnk");
            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutLocation);

            shortcut.TargetPath = @"C:\TEMP\SmartSearch\SmartSearchResetRes.exe";
            shortcut.IconLocation = @"C:\TEMP\SmartSearch\SmartSearchResetRes.exe";
            shortcut.Save();
        }

        private void Button1_Click(object sender, EventArgs e) => Close();

        private void SmartSearchWin10Fix_Load(object sender, EventArgs e)
        {
            if (Screen.AllScreens.Length > 1)
            {
                MessageBox.Show("Please disconnect from docking station, and relaunch application.", "Multiple Monitors Detected");
                Close();
            }
        }
    }
}